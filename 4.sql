-- INSERT va UPDATE so'rovlari
-- Grant INSERT va UPDATE huquqlarini berish
GRANT INSERT, UPDATE ON TABLE rental TO rental;

-- INSERT so'rovi orqali yangi qator kiritish
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2023-11-27', 1, 1, NULL, 1);

-- UPDATE so'rovi orqali ma'lumotni yangilash
UPDATE rental
SET return_date = '2023-12-05'
WHERE rental_id = 1;
