-- rental jadvalidagi bitta qator o'zgartirish
UPDATE rental
SET rental_duration = 7, -- O'zgartirish kerak bo'lgan ustun
    rental_rate = 2.5   -- O'zgartirish kerak bo'lgan boshqa ustun
WHERE rental_id = 1;    -- Yangilanayotgan qatorning identifikatori (primary key)

-- INSERT huquqini bekor qilish
REVOKE INSERT ON TABLE rental FROM rental;

-- Yangi ma'lumot kiritishni sinab ko'rish, xatolik keladi
INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2023-11-27', 1, 1, NULL, 1);
