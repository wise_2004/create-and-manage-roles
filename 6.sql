DO $$ 
DECLARE 
    customer_id INTEGER;
    role_name VARCHAR(255);
BEGIN 
    -- Mijozlardan birini tanlang
    SELECT customer_id INTO customer_id FROM customer LIMIT 1;
    
    -- Shaxsiy huquqlar va rol yaratish
    role_name := 'client_' || (SELECT first_name FROM customer WHERE customer_id = id) || '_' || (SELECT last_name FROM customer WHERE customer_id = id);
    EXECUTE 'CREATE ROLE ' || role_name;
    EXECUTE 'GRANT SELECT ON TABLE rental, payment TO ' || role_name;
    EXECUTE 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLE rental, payment TO ' || role_name;
    EXECUTE 'GRANT ' || role_name || ' TO rentaluser';
END $$;

-- Mijozning o'z ma'lumotlarini ko'rish
SET ROLE client_FirstName_LastName; -- Mijozning ismi va familiyasi
SELECT * FROM rental WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'FirstName' AND last_name = 'LastName');
